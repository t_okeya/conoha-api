<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

/**
 * Class ConohaTest.
 */
class ConohaTest extends Orchestra\Testbench\TestCase
{

    /**
     * Get package providers.
     *
     * @param  \Illuminate\Foundation\Application $app
     * @return array
     */
    protected function getPackageProviders($app)
    {
        return [
            'Kyon2\Conoha\Providers\ConohaServiceProvider',
        ];
    }

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('conoha', array(
            'tenant_id'     => 'tenant_id',
            'username'      => 'user_name',
            'password'      => 'password',
        ));
        $app['config']->set('cache.prefix', 'testing_laravel5_conoha_api');
    }

    /**
     * Setup the test environment.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Some codes will be here...
    }

    /**
     * Clean up the testing environment before the next test.
     *
     * @return void
     */
    public function tearDown()
    {
        Cache::forget('cache_key');
        Mockery::close();
    }


    // --------------------------------------------------
    // Identity API v2.0
    // --------------------------------------------------
    /**
     * Test caching auth tokens.
     */
    public function testCachingTokens()
    {
        $dummyToken = 'dummy_token';

        $tokenResponse = Mockery::mock('tokenResponse');
        $tokenResponse->shouldReceive('getBody')
            ->once()
            ->andReturn(json_encode(array(
                'access' => array(
                    'token' => array(
                        'id'      => $dummyToken,
                        'expires' => (string)Carbon::now()->addDays(1),
                    )
                )
            )));

        $client = Mockery::mock('client');
        $client->shouldReceive('post')
            ->with(
                'https://identity.tyo1.conoha.io/v2.0/tokens',
                array(
                    'json' => array(
                        'auth' => array(
                            'tenantId'            => config('conoha.tenant_id'),
                            'passwordCredentials' => array(
                                'username' => config('conoha.username'),
                                'password' => config('conoha.password'),
                            )
                        )
                    )
                )
            )
            ->once()
            ->andReturn($tokenResponse);

        $api = new \Kyon2\Conoha\Api\ObjectStorage('cache_key', $client);
        ($api);
        $actual = Cache::get('cache_key')->id;
        $expected = 'dummy_token';
        $this->assertEquals($expected, $actual);
    }


    // --------------------------------------------------
    // Block Storage API v2
    // --------------------------------------------------
    /**
     * Test the getTypes method.
     */
    public function testGetTypes()
    {
        $dummyToken    = 'dummy_token';

        $tokenResponse = Mockery::mock('tokenResponse');
        $tokenResponse->shouldReceive('getBody')
            ->once()
            ->andReturn(json_encode(array(
                'access' => array(
                    'token' => array(
                        'id'      => $dummyToken,
                        'expires' => (string)Carbon::now()->addDays(1),
                    )
                ),
            )));

        $client = Mockery::mock('client');
        $client->shouldReceive('post')
            ->with(
                'https://identity.tyo1.conoha.io/v2.0/tokens',
                array(
                    'json' => array(
                        'auth' => array(
                            'tenantId'            => config('conoha.tenant_id'),
                            'passwordCredentials' => array(
                                'username' => config('conoha.username'),
                                'password' => config('conoha.password'),
                            )
                        )
                    )
                )
            )
            ->once()
            ->andReturn($tokenResponse);

        $listResponse = Mockery::mock('listResponse');
        $listResponse->shouldReceive('getBody')
            ->once()
            ->andReturn(json_encode('It\'s a dummy response!'));

        $client->shouldReceive('get')
            ->with(
                config('conoha.tenant_id') . '/types',
                array(
                    'headers' => array(
                        'Accept' => 'application/json',
                        'X-Auth-Token' => $dummyToken,
                    )
                )
            )
            ->once()
            ->andReturn($listResponse);

        $api = new \Kyon2\Conoha\Api\BlockStorage(null, $client);
        try
        {
            $actual = $api->getTypes();
        }
        catch (\Exception $e)
        {

        }
        $expected = 'It\'s a dummy response!';
        $this->assertEquals($expected, $actual);
    }


    // --------------------------------------------------
    // Compute API v2
    // --------------------------------------------------
    /**
     * Test the getServers method.
     */
    public function testGetServers()
    {
        $dummyToken    = 'dummy_token';

        $tokenResponse = Mockery::mock('tokenResponse');
        $tokenResponse->shouldReceive('getBody')
            ->once()
            ->andReturn(json_encode(array(
                'access' => array(
                    'token' => array(
                        'id'      => $dummyToken,
                        'expires' => (string)Carbon::now()->addDays(1),
                    )
                ),
            )));

        $client = Mockery::mock('client');
        $client->shouldReceive('post')
            ->with(
                'https://identity.tyo1.conoha.io/v2.0/tokens',
                array(
                    'json' => array(
                        'auth' => array(
                            'tenantId'            => config('conoha.tenant_id'),
                            'passwordCredentials' => array(
                                'username' => config('conoha.username'),
                                'password' => config('conoha.password'),
                            )
                        )
                    )
                )
            )
            ->once()
            ->andReturn($tokenResponse);

        $listResponse = Mockery::mock('listResponse');
        $listResponse->shouldReceive('getBody')
            ->once()
            ->andReturn(json_encode('It\'s a dummy response!'));

        $client->shouldReceive('get')
            ->with(
                config('conoha.tenant_id') . '/servers',
                array(
                    'headers' => array(
                        'Accept' => 'application/json',
                        'X-Auth-Token' => $dummyToken,
                    )
                )
            )
            ->once()
            ->andReturn($listResponse);

        $api = new \Kyon2\Conoha\Api\Compute(null, $client);
        try
        {
            $actual = $api->getServers();
        }
        catch (\Exception $e)
        {

        }
        $expected = 'It\'s a dummy response!';
        $this->assertEquals($expected, $actual);
    }


    // --------------------------------------------------
    // ObjectStorage API v1
    // --------------------------------------------------
    /**
     * Test the getContainer method.
     */
    public function testGetContainer()
    {
        $dummyToken    = 'dummy_token';
        $containerName = 'dummy_container';

        $tokenResponse = Mockery::mock('tokenResponse');
        $tokenResponse->shouldReceive('getBody')
            ->once()
            ->andReturn(json_encode(array(
                'access' => array(
                    'token' => array(
                        'id'      => $dummyToken,
                        'expires' => (string)Carbon::now()->addDays(1),
                    )
                ),
            )));

        $client = Mockery::mock('client');
        $client->shouldReceive('post')
            ->with(
                'https://identity.tyo1.conoha.io/v2.0/tokens',
                array(
                    'json' => array(
                        'auth' => array(
                            'tenantId'            => config('conoha.tenant_id'),
                            'passwordCredentials' => array(
                                'username' => config('conoha.username'),
                                'password' => config('conoha.password'),
                            )
                        )
                    )
                )
            )
            ->once()
            ->andReturn($tokenResponse);

        $listResponse = Mockery::mock('listResponse');
        $listResponse->shouldReceive('getBody')
            ->once()
            ->andReturn(json_encode('It\'s a dummy response!'));

        $client->shouldReceive('get')
            ->with(
                'nc_' . config('conoha.tenant_id') . '/' . $containerName,
                array(
                    'headers' => array(
                        'Accept' => 'application/json',
                        'X-Auth-Token' => $dummyToken,
                    )
                )
            )
            ->once()
            ->andReturn($listResponse);

        $api = new \Kyon2\Conoha\Api\ObjectStorage(null, $client);

        try
        {
            $actual = $api->getContainer($containerName);
        }
        catch (\Exception $e)
        {

        }

        $expected = 'It\'s a dummy response!';
        $this->assertEquals($expected, $actual);
    }

}
