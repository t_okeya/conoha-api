<?php

namespace Kyon2\Conoha\Providers;

use Illuminate\Support\ServiceProvider;
use Kyon2\Conoha\ConohaManager;

/**
 * Class ConohaServiceProvider.
 *
 * @package Kyon2\Conoha\Providers
 */
class ConohaServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes(array(
            __DIR__ . '/../../config/conoha.php' => config_path('conoha.php'),
        ));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('conoha', function ($app) {
            return new ConohaManager($app);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['conoha'];
    }

}
