<?php

namespace Kyon2\Conoha\Api;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;

/**
 * Class API.
 *
 * @package Kyon2\Conoha\Api
 */
abstract class API
{
    /** string protcol. */
    const PROTOCOL = 'https://';
    /** string base uri. */
    const BASE_URI = '.tyo1.conoha.io';

    /** string v1. */
    const V1 = 'v1';
    /** string v2. */
    const V2 = 'v2';

    /** @var \GuzzleHttp\Client $_client GuzzleHttp Client. */
    protected $_client;
    /** @var string $_token Auth token. */
    protected $_token;
    /** @var \Carbon\Carbon $_tokenExpiresAt Token expiration datetime. */
    protected $_tokenExpiresAt;
    /** @var string|null $_tokenCacheKey Cache key to store an auth token. If null is set, the token is not cached. */
    protected $_tokenCacheKey;

    /**
     * API constructor.
     *
     * @param $baseUri
     * @param null $tokenCacheKey
     * @param null $client
     */
    public function __construct($baseUri, $tokenCacheKey = null, $client = null)
    {
        $this->_client        = is_null($client) ? new Client(['base_uri' => $baseUri]) : $client;
        $this->_tokenCacheKey = $tokenCacheKey;
        $this->_setToken();
    }

    /**
     * Set an auth token.
     */
    protected function _setToken()
    {
        // check if the token is cached
        if (isset($this->_tokenCacheKey) && Cache::has($this->_tokenCacheKey))
        {
            $token                 = Cache::get($this->_tokenCacheKey);
            $this->_token          = $token->id;
            $this->_tokenExpiresAt = Carbon::parse($token->expires);
        }

        // validate the token
        if ($this->_hasValidToken())
        {
            return;
        }

        // if the token is not cached or expired, get a new auth token
        $requestBody = array(
            'auth' => array(
                'tenantId'            => config('conoha.tenant_id'),
                'passwordCredentials' => array(
                    'username' => config('conoha.username'),
                    'password' => config('conoha.password'),
                ),
            ),
        );
        $authUri = self::PROTOCOL . 'identity' . self::BASE_URI . '/v2.0/tokens';
        $response = $this->_client->post(
            $authUri, array('json' => $requestBody)
        );
        $responseBody = json_decode($response->getBody());

        if (isset($this->_tokenCacheKey))
        {
            // cache the token
            Cache::put($this->_tokenCacheKey, $responseBody->access->token, 60 * 24);
        }

        // set the token to properties
        $this->_token          = $responseBody->access->token->id;
        $this->_tokenExpiresAt = Carbon::parse($responseBody->access->token->expires);
    }

    /**
     * Validate an auth token in properties.
     *
     * @return boolean false if the token is not set or expired, true otherwise.
     */
    protected function _hasValidToken()
    {
        if (!isset($this->_token) || !isset($this->_tokenExpiresAt))
        {
            return false;
        }
        return $this->_tokenExpiresAt > Carbon::now();
    }

}
