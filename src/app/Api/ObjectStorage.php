<?php

namespace Kyon2\Conoha\Api;

use Kyon2\Conoha\Traits\APIUri;

/**
 * Class ObjectStorage.
 *
 * @package Kyon2\Conoha\Api
 */
class ObjectStorage extends API
{

    use APIUri;

    /**
     * ObjectStorage constructor.
     *
     * @param null $tokenCacheKey
     * @param null $client
     */
    public function __construct($tokenCacheKey = null, $client = null)
    {
        $baseUri = $this->setUri(parent::PROTOCOL, 'object-storage', parent::BASE_URI, parent::V1);
        parent::__construct($baseUri, $tokenCacheKey, $client);
    }

    /**
     * Get Container Info.
     *
     * @param $container
     * @return mixed
     * @throws \Exception
     */
    public function getContainer($container)
    {
        if (!$this->_hasValidToken())
        {
            $this->_setToken();
        }

        try
        {
            $response = $this->_client->get(
                'nc_' . config('conoha.tenant_id') . '/' . $container,
                array(
                    'headers' => array(
                        'Accept'       => 'application/json',
                        'X-Auth-Token' => $this->_token,
                ))
            );
        }
        catch (\Exception $e)
        {
            throw new \Exception('Failed to get a container info. {container: ' . $container . '}', $e->getCode(), $e);
        }

        return json_decode($response->getBody());
    }

    /**
     * Get Object Info.
     *
     * @param $container
     * @param $object
     * @return mixed
     * @throws \Exception
     */
    public function getObject($container, $object)
    {
        if (!$this->_hasValidToken())
        {
            $this->_setToken();
        }

        try
        {
            $response = $this->_client->get(
                'nc_' . config('conoha.tenant_id') . '/' . $container . '/' . $object,
                array(
                    'headers' => array(
                        'Accept'       => 'application/json',
                        'X-Auth-Token' => $this->_token,
                    ))
            );
        }
        catch (\Exception $e)
        {
            throw new \Exception('Failed to get a object info. {container: ' . $container . ', object: ' . $object . '}', $e->getCode(), $e);
        }

        return json_decode($response->getBody());
    }

}
