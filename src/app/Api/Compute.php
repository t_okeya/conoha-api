<?php

namespace Kyon2\Conoha\Api;

use Kyon2\Conoha\Traits\APIUri;

/**
 * Class Compute.
 *
 * @package Kyon2\Conoha\Api
 */
class Compute extends API
{

    use APIUri;

    /**
     * Compute constructor.
     *
     * @param null $tokenCacheKey
     * @param null $client
     */
    public function __construct($tokenCacheKey = null, $client = null)
    {
        $baseUri = $this->setUri(parent::PROTOCOL, 'compute', parent::BASE_URI, parent::V2);
        parent::__construct($baseUri, $tokenCacheKey, $client);
    }

    /**
     * Get List of VM Plan.
     *
     * @return mixed
     * @throws \Exception
     */
    public function getFlavors()
    {
        if (!$this->_hasValidToken())
        {
            $this->_setToken();
        }

        try
        {
            $response = $this->_client->get(
                config('conoha.tenant_id') . '/flavors',
                array(
                    'headers' => array(
                        'Accept'       => 'application/json',
                        'X-Auth-Token' => $this->_token,
                    ))
            );
        }
        catch (\Exception $e)
        {
            throw new \Exception('Failed to get list of vm plan.', $e->getCode(), $e);
        }

        return json_decode($response->getBody());
    }

    /**
     * Get List of Detail VM Plan.
     *
     * @return mixed
     * @throws \Exception
     */
    public function getFlavors4Detail()
    {
        if (!$this->_hasValidToken())
        {
            $this->_setToken();
        }

        try
        {
            $response = $this->_client->get(
                config('conoha.tenant_id') . '/flavors/detail',
                array(
                    'headers' => array(
                        'Accept'       => 'application/json',
                        'X-Auth-Token' => $this->_token,
                    ))
            );
        }
        catch (\Exception $e)
        {
            throw new \Exception('Failed to get a list of detail vm plan.', $e->getCode(), $e);
        }

        return json_decode($response->getBody());
    }

    /**
     * Get Detail of VM Plan.
     *
     * @param $flavorId
     * @return mixed
     * @throws \Exception
     */
    public function getFlavor($flavorId)
    {
        if (!$this->_hasValidToken())
        {
            $this->_setToken();
        }

        try
        {
            $response = $this->_client->get(
                config('conoha.tenant_id') . '/flavors/' . $flavorId,
                array(
                    'headers' => array(
                        'Accept'       => 'application/json',
                        'X-Auth-Token' => $this->_token,
                    ))
            );
        }
        catch (\Exception $e)
        {
            throw new \Exception('Failed to get a detail of vm plan. {flavor_id: ' . $flavorId . '}', $e->getCode(), $e);
        }

        return json_decode($response->getBody());
    }

    /**
     * Get List of VM.
     *
     * @return mixed
     * @throws \Exception
     */
    public function getServers()
    {
        if (!$this->_hasValidToken())
        {
            $this->_setToken();
        }

        try
        {
            $response = $this->_client->get(
                config('conoha.tenant_id') . '/servers',
                array(
                    'headers' => array(
                        'Accept'       => 'application/json',
                        'X-Auth-Token' => $this->_token,
                ))
            );
        }
        catch (\Exception $e)
        {
            throw new \Exception('Failed to get a list of vm.', $e->getCode(), $e);
        }

        return json_decode($response->getBody());
    }

    /**
     * Get List of Detail VM .
     *
     * @return mixed
     * @throws \Exception
     */
    public function getServers4Detail()
    {
        if (!$this->_hasValidToken())
        {
            $this->_setToken();
        }

        try
        {
            $response = $this->_client->get(
                config('conoha.tenant_id') . '/servers/detail',
                array(
                    'headers' => array(
                        'Accept'       => 'application/json',
                        'X-Auth-Token' => $this->_token,
                    ))
            );
        }
        catch (\Exception $e)
        {
            throw new \Exception('Failed to get a list of detail vm.', $e->getCode(), $e);
        }

        return json_decode($response->getBody());
    }

    /**
     * Get Detail of VM.
     *
     * @param $serverId
     * @return mixed
     * @throws \Exception
     */
    public function getServer($serverId)
    {
        if (!$this->_hasValidToken())
        {
            $this->_setToken();
        }

        try
        {
            $response = $this->_client->get(
                config('conoha.tenant_id') . '/servers/' . $serverId,
                array(
                    'headers' => array(
                        'Accept'       => 'application/json',
                        'X-Auth-Token' => $this->_token,
                    ))
            );
        }
        catch (\Exception $e)
        {
            throw new \Exception('Failed to get a detail of vm. {server_id: ' . $serverId . '}', $e->getCode(), $e);
        }

        return json_decode($response->getBody());
    }

}
