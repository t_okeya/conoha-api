<?php

namespace Kyon2\Conoha\Api;

use Kyon2\Conoha\Traits\APIUri;

/**
 * Class BlockStorage.
 *
 * @package Kyon2\Conoha\Api
 */
class BlockStorage extends API
{
    use APIUri;

    /**
     * BlockStorage constructor.
     *
     * @param null $tokenCacheKey
     * @param null $client
     */
    public function __construct($tokenCacheKey = null, $client = null)
    {
        $baseUri = $this->setUri(parent::PROTOCOL, 'block-storage', parent::BASE_URI, parent::V2);
        parent::__construct($baseUri, $tokenCacheKey, $client);
    }

    /**
     * Get List of Volume Type.
     *
     * @return mixed
     * @throws \Exception
     */
    public function getTypes()
    {
        if (!$this->_hasValidToken())
        {
            $this->_setToken();
        }

        try
        {
            $response = $this->_client->get(
                config('conoha.tenant_id') . '/types',
                array(
                    'headers' => array(
                        'Accept'       => 'application/json',
                        'X-Auth-Token' => $this->_token,
                ))
            );
        }
        catch (\Exception $e)
        {
            throw new \Exception('Failed to get a list of volume type.', $e->getCode(), $e);
        }

        return json_decode($response->getBody());
    }

    /**
     * Get Detail of Volume Type.
     *
     * @param $volumeTypeId
     * @return mixed
     * @throws \Exception
     */
    public function getType($volumeTypeId)
    {
        if (!$this->_hasValidToken())
        {
            $this->_setToken();
        }

        try
        {
            $response = $this->_client->get(
                config('conoha.tenant_id') . '/types/' . $volumeTypeId,
                array(
                    'headers' => array(
                        'Accept'       => 'application/json',
                        'X-Auth-Token' => $this->_token,
                    ))
            );
        }
        catch (\Exception $e)
        {
            throw new \Exception('Failed to get a detail of volume type. {volume_type_id: ' . $volumeTypeId . '}', $e->getCode(), $e);
        }

        return json_decode($response->getBody());
    }

    /**
     * Get List of Volume.
     *
     * @return mixed
     * @throws \Exception
     */
    public function getVolumes()
    {
        if (!$this->_hasValidToken())
        {
            $this->_setToken();
        }

        try
        {
            $response = $this->_client->get(
                config('conoha.tenant_id') . '/volumes',
                array(
                    'headers' => array(
                        'Accept'       => 'application/json',
                        'X-Auth-Token' => $this->_token,
                    ))
            );
        }
        catch (\Exception $e)
        {
            throw new \Exception('Failed to get a list of volume.', $e->getCode(), $e);
        }

        return json_decode($response->getBody());
    }

    /**
     * Get List of Detail Volume.
     *
     * @return mixed
     * @throws \Exception
     */
    public function getVolumes4Detail()
    {
        if (!$this->_hasValidToken())
        {
            $this->_setToken();
        }

        try
        {
            $response = $this->_client->get(
                config('conoha.tenant_id') . '/volumes/detail',
                array(
                    'headers' => array(
                        'Accept'       => 'application/json',
                        'X-Auth-Token' => $this->_token,
                    ))
            );
        }
        catch (\Exception $e)
        {
            throw new \Exception('Failed to get a list of detail volume.', $e->getCode(), $e);
        }

        return json_decode($response->getBody());
    }

    /**
     * Get Detail of Volume.
     *
     * @param $volumeId
     * @return mixed
     * @throws \Exception
     */
    public function getVolume($volumeId)
    {
        if (!$this->_hasValidToken())
        {
            $this->_setToken();
        }

        try
        {
            $response = $this->_client->get(
                config('conoha.tenant_id') . '/volumes/' . $volumeId,
                array(
                    'headers' => array(
                        'Accept'       => 'application/json',
                        'X-Auth-Token' => $this->_token,
                    ))
            );
        }
        catch (\Exception $e)
        {
            throw new \Exception('Failed to get a detail of volume. {volume_id: ' . $volumeId . '}', $e->getCode(), $e);
        }

        return json_decode($response->getBody());
    }

}
