<?php

namespace Kyon2\Conoha\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Conoha.
 *
 * @package Kyon2\Conoha\Facades
 */
class Conoha extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'conoha';
    }
}