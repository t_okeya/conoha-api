<?php

namespace Kyon2\Conoha;

use Illuminate\Support\Manager;

/**
 * Class ConohaManager.
 *
 * @package Kyon2\Conoha
 */
class ConohaManager extends Manager
{

    /**
     * @return mixed
     */
    protected function createBlockStorageDriver()
    {
        return $this->buildProvider('Kyon2\Conoha\Api\BlockStorage');
    }

    /**
     * @return mixed
     */
    protected function createComputeDriver()
    {
        return $this->buildProvider('Kyon2\Conoha\Api\Compute');
    }

    /**
     * @return mixed
     */
    protected function createObjectStorageDriver()
    {
        return $this->buildProvider('Kyon2\Conoha\Api\ObjectStorage');
    }

    /**
     * @param $provider
     * @param null $tokenCacheKey
     * @param null $client
     * @return mixed
     */
    public function buildProvider($provider, $tokenCacheKey = null, $client = null)
    {
        return new $provider($tokenCacheKey, $client);
    }

    /**
     * Get the default driver name.
     *
     * @return string
     */
    public function getDefaultDriver()
    {
        throw new \InvalidArgumentException('No Conoha driver was specified.');
    }
}