<?php

namespace Kyon2\Conoha\Traits;

trait APIUri
{
    /**
     * set base uri.
     *
     * @param $protocol
     * @param $functionName
     * @param $baseUri
     * @param $version
     * @return string
     */
    private function setUri($protocol, $functionName, $baseUri, $version)
    {
        return $protocol . $functionName . $baseUri . '/' . $version . '/';
    }

}
