# Laravel 5 ConoHa API


# Installation

To install the PHP client library using Composer

```sh
composer require kyon2/laravel5-conoha-api
```

To use the package, register the service provider in `config/app.php`.

```php
'providers' => [
    // ...
    Kyon2\Conoha\Providers\ConohaServiceProvider::class,
]
```


# Configuration

To configure your connection settings, execute the following command.

```sh
php artisan vendor:publish --provider="Kyon2\Conoha\Providers\ConohaServiceProvider"
```

Then set the following environment variables in `.env` file.

```
CONOHA_TENANT_ID
CONOHA_USERNAME
CONOHA_PASSWORD
```


# Features

```php
# Block Storage API v2
GET /v2/{tenant_id}/types
GET /v2/{tenant_id}/types/{volume_type_id}
GET /v2/{tenant_id}/volumes
GET /v2/{tenant_id}/volumes/{volume_id}

# Compute API v2
GET /v2/{tenant_id}/flavors
GET /v2/{tenant_id}/flavors/detail
GET /v2/{tenant_id}/flavors/{flavor_id}
GET /v2/{tenant_id}/servers
GET /v2/{tenant_id}/servers/detail
GET /v2/{tenant_id}/servers/{server_id}

# ObjectStorage API v1
GET /v1/nc_​{account}​/​{container}​
GET /v1/nc_​{account}​/​{container}​/​{object}​
```

have only one thing one can do "reference-related".


# Usage

## Create the Instance

To use the ConohaAPI First of all you have to create the ConohaAPI instance.

### Example

```php
use Conoha;
```

```php
public function __construct()
{
    $this->blockStorage = Conoha::driver('BlockStorage');
}
```

## Get a list of Volume Types.

### Example

```php
$results = $this->blockStorage->getTypes();
```
